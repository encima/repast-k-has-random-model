/**
 * 
 */
package com.encima.khas;

import static repast.simphony.essentials.RepastEssentials.*;
import java.util.Iterator;

import repast.simphony.engine.environment.RunEnvironment;
import repast.simphony.engine.schedule.ScheduleParameters;
import repast.simphony.query.space.continuous.ContinuousWithin;
import repast.simphony.space.continuous.ContinuousSpace;

/**
 * @author encima
 *
 */
public class DANode extends Node {

	/**
	 * @param nodeID
	 * @param transMethod
	 * @param maxStorage
	 * @param space
	 */
	public DANode(int nodeID, String transMethod, int maxStorage,
			ContinuousSpace<Object> space) {
		super(nodeID, transMethod, maxStorage, space);
		RunEnvironment inst = RunEnvironment.getInstance();
		ScheduleParameters sp = ScheduleParameters.createOneTime(1);
		RunEnvironment.getInstance().getCurrentSchedule().schedule(sp, this, "linkDPNodes");
	}
	
//	Searches for Data Processing nodes, adding them as receivers.
	public void linkDPNodes() {
		Iterator iter = new ContinuousWithin(space, this, this.getTransRange()).query().iterator();
		while(iter.hasNext()) {
			Object n = iter.next();
			if(n instanceof DPNode) {
				((DPNode) n).receiveBroadcast(this);
			}
		}
	}
}
