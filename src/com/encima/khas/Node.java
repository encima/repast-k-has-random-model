/**
 * 
 */
package com.encima.khas;

import static repast.simphony.essentials.RepastEssentials.CreateEdge;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Iterator;
import java.util.Properties;
import java.util.Vector;

import repast.simphony.engine.environment.RunEnvironment;
import repast.simphony.engine.schedule.ScheduleParameters;
import repast.simphony.query.space.continuous.ContinuousWithin;
import repast.simphony.space.continuous.ContinuousSpace;
import edu.uci.ics.jung.visualization.renderers.Renderer.Edge;

/**
 * @author encima
 *
 */
public class Node {

	private String projection = "K-HAS/sensor network";
	
	protected Vector<DwCArchive> archives;
	
	private int hopsToDA = -1;
	
	int nodeID;
	String transMethod;
	double transRange;
	double transRate; //in seconds
	
	int usedStorage = 0;
	int maxStorage;
	
	private SendState sendState;
	
	protected ContinuousSpace<Object> space;
	
	Properties transProps;
	String propsFile = "properties/rf.properties";

	Node receiver = null;

	protected Node getReceiver() {
		return receiver;
	}

	protected void setReceiver(Node receiver) {
		this.receiver = receiver;
	}

	public Node(int nodeID, String transMethod, int maxStorage,
			ContinuousSpace<Object> space) {
		super();
		archives = new Vector<DwCArchive>();
		transProps = new Properties();
		this.nodeID = nodeID;
		this.transMethod = transMethod;
		this.maxStorage = maxStorage;
		this.setSpace(space);
		
//		Load the correct properties file based on the transmission method setting, using RF as default
//		Switch statement can't be used as Strings are only supported in 1.7 and repast has OpenGL issues in 1.7
		if(transMethod.toLowerCase	().equals("rf"))
			propsFile = "properties/rf.properties";
		else if(transMethod.toLowerCase().equals("zigbee"))
			propsFile = "properties/zigbee.properties";
		else if(transMethod.toLowerCase().equals("wi-fi"))
			propsFile = "properties/rf.properties";
		
		try {
			transProps.load(new FileInputStream(propsFile));
			this.setTransRange(Double.parseDouble(transProps.getProperty("range")));
			this.setTransRate(Double.parseDouble(transProps.getProperty("rate")));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void receive(DwCArchive recvd, int transTime) {
		int tick = (int) RunEnvironment.getInstance().getCurrentSchedule().getTickCount();
		if(transTime <= tick) {
			this.archives.add(recvd);
			recvd.addRoute(this);
			if(this instanceof DPNode) {
//				On receipt of a set, images are processed, assuming 3 images takes 90 seconds(ticks)
				RunEnvironment inst = RunEnvironment.getInstance();
				ScheduleParameters sp = ScheduleParameters.createOneTime(inst.getCurrentSchedule().getTickCount(),
						ScheduleParameters.RANDOM_PRIORITY, 90);
				Object[] obj = {recvd};
				RunEnvironment.getInstance().getCurrentSchedule().schedule(sp, this, "processSet", obj);
			}else if(this instanceof DANode) {
				if(recvd.getContents().equals("INTERESTING")) {
					System.out.printf("DANode Received: took %d hours for INTERESTING IMAGE from original node DC %d\n", ((tick - recvd.images.get(0).getTakenAt())/60)/60, recvd.route.get(0).getNodeID());
					for(Node n: recvd.route.values()) {
						System.out.println(n.getNodeID());
					}
					System.out.println("---------");
				}else{
					System.out.printf("DANode Received: took %d hours from DC %d\n", ((tick - recvd.images.get(0).getTakenAt())/60)/60, recvd.route.get(0).getNodeID());
				}
			}
		}
	}
	
	public void queryNeighbours() {
		if(!(this instanceof DANode)) {
			Iterator iter = new ContinuousWithin(getSpace(), this, this.getTransRange()).query().iterator();
			while(iter.hasNext()) {
				Node obj = (Node) iter.next();
				obj.receiveBroadcast(this);
			}
		}
	}
	
	public void receiveBroadcast(Node requester) {
		this.setReceiver(requester);
		this.setHopsToDA(requester.getHopsToDA() + 1);
		CreateEdge(getProjection(), this, requester, 1.0);		
		System.out.println("Node " + this.getNodeID() + " is " + this.getHopsToDA() + " hops away!");
			Iterator iter = new ContinuousWithin(getSpace(), this, this.getTransRange()).query().iterator();
			int count = 0;
			while(iter.hasNext()) {
				Node obj = (Node) iter.next();
//				Check if node does not have a link already. If it does, add new link only if it
//				is fewer hops away
				if((obj.getHopsToDA() == -1 && obj instanceof DCNode) || 
						(obj instanceof DCNode && obj.getHopsToDA() != -1 && 
						obj.getHopsToDA() + 1 > this.getHopsToDA() + 1)) {
					obj.receiveBroadcast(this);
					count++;
				}
			}
	}

	protected Vector<DwCArchive> getArchives() {
		return archives;
	}

	protected void setArchives(Vector<DwCArchive> archives) {
		this.archives = archives;
	}

	public int getNodeID() {
		return nodeID;
	}

	protected void setNodeID(int nodeID) {
		this.nodeID = nodeID;
	}

	protected String getTransMethod() {
		return transMethod;
	}

	protected void setTransMethod(String transMethod) {
		this.transMethod = transMethod;
	}

	protected double getTransRange() {
		return transRange;
	}

	protected void setTransRange(double transRange) {
		this.transRange = transRange;
	}

	protected double getTransRate() {
		return transRate;
	}

	protected void setTransRate(double transRate) {
		this.transRate = transRate;
	}

	protected int getUsedStorage() {
		return usedStorage;
	}

	protected void setUsedStorage(int usedStorage) {
		this.usedStorage = usedStorage;
	}

	protected int getMaxStorage() {
		return maxStorage;
	}

	protected void setMaxStorage(int maxStorage) {
		this.maxStorage = maxStorage;
	}

	protected Properties getTransProps() {
		return transProps;
	}

	protected void setTransProps(Properties transProps) {
		this.transProps = transProps;
	}

	public String getProjection() {
		return projection;
	}

	public void setProjection(String projection) {
		this.projection = projection;
	}

	SendState getSendState() {
		return sendState;
	}

	void setSendState(SendState sendState) {
		this.sendState = sendState;
	}

	protected void setHopsToDA(int hopsToDA) {
		this.hopsToDA = hopsToDA;
	}

	protected int getHopsToDA() {
		return hopsToDA;
	}

	ContinuousSpace<Object> getSpace() {
		return space;
	}

	void setSpace(ContinuousSpace<Object> space) {
		this.space = space;
	}


}
