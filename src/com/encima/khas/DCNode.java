/**
 * 
 */
package com.encima.khas;

import static repast.simphony.essentials.RepastEssentials.*;

import java.util.Date;
import java.util.Iterator;
import java.util.Vector;

import repast.simphony.engine.environment.RunEnvironment;
import repast.simphony.engine.schedule.ScheduleParameters;
import repast.simphony.engine.schedule.ScheduledMethod;
import repast.simphony.query.space.continuous.ContinuousWithin;
import repast.simphony.space.continuous.ContinuousSpace;
import repast.simphony.space.continuous.NdPoint;
import repast.simphony.space.graph.Network;

/**
 * @author encima
 *
 */
public class DCNode extends Node {

	Vector<NdPoint> dpLocs;
	
	/**
	 * @param nodeID
	 * @param transMethod
	 * @param maxStorage
	 * @param space
	 */
	public DCNode(int nodeID, String transMethod, int maxStorage,
			ContinuousSpace<Object> space, Vector<NdPoint> dpLocs) {
		super(nodeID, transMethod, maxStorage, space);
//		RunEnvironment inst = RunEnvironment.getInstance();
//		ScheduleParameters sp = ScheduleParameters.createOneTime(10);
//		RunEnvironment.getInstance().getCurrentSchedule().schedule(sp, this, "linkNodes");
	}
	
	@ScheduledMethod (start = 100, interval = 9586, priority = 1.7976931348623157E308d)
	public void takeImage() {
		if(Math.random() * 10 <= 1.1) {
			Vector<Image> taken = new Vector<Image>();
			int tick = (int) RunEnvironment.getInstance().getCurrentSchedule().getTickCount();
			for(int i = 0; i < 3; i++) {
				taken.add(new Image(this.getNodeID(), tick, new String("IMG" + i + "JPG")));
			}
			this.archives.add(new DwCArchive(taken, this));
		}
	}
	
	@ScheduledMethod (start = 10, interval = 10000, priority = 1.7976931348623157E308d)
	public void sendImage() {
		if(this.getSendState() != null) {
			int tick = (int) RunEnvironment.getInstance().getCurrentSchedule().getTickCount();
//			On preparation of sending the file, calculate the send time from the start
			DwCArchive toSend = (DwCArchive) this.getSendState().getFile();
			if(this.getSendState().getTransTime() == -1) {
				int transTime = (int) (tick + (toSend.getSize() / this.getTransRate()));
				this.getSendState().setTransTime(transTime);
			} else if(this.getSendState().getTransTime() <= tick) {
				if(this.getReceiver() != null) {
					this.getReceiver().receive(toSend, this.getSendState().getTransTime());
					archives.remove(toSend);
					this.setSendState(null);
				}
			}
		}else if(this.archives.size() > 1){
			this.setSendState(new SendState(this.archives.firstElement(), -1));
		}
	}
	
//	Repast seems to leak memory like a mother, 2MB for every few thousand ticks
//	@ScheduledMethod (start = 1000000, interval = 1000000, priority = ScheduleParameters.LAST_PRIORITY)
//	public void gc() {
//		System.gc();
//	}
}
