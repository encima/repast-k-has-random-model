package com.encima.khas;



public class SendState {

	private boolean sending;
	private Object file;
	private int transTime;
	
	public SendState(Object file, int transTime) {
		this.setFile(file);
		this.setTransTime(transTime);
		this.setSending(true);
	}

	boolean isSending() {
		return sending;
	}

	void setSending(boolean sending) {
		this.sending = sending;
		if(sending  == false) {
			this.setFile(null);
			this.setTransTime(0);
		}
	}

	Object getFile() {
		return file;
	}
	void setFile(Object file) {
		this.file = file;
	}

	int getTransTime() {
		return transTime;
	}

	void setTransTime(int transTime) {
		this.transTime = transTime;
	}

}