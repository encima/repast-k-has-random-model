/**
 * 
 */
package com.encima.khas;

import java.util.Iterator;
import java.util.Vector;

import repast.simphony.context.Context;
import repast.simphony.context.DefaultContext;
import repast.simphony.context.space.continuous.ContinuousSpaceFactory;
import repast.simphony.context.space.continuous.ContinuousSpaceFactoryFinder;
import repast.simphony.context.space.graph.NetworkBuilder;
import repast.simphony.dataLoader.ContextBuilder;
import repast.simphony.engine.environment.RunEnvironment;
import repast.simphony.parameter.Parameters;
import repast.simphony.query.space.continuous.ContinuousWithin;
import repast.simphony.space.continuous.ContinuousSpace;
import repast.simphony.space.continuous.NdPoint;
import repast.simphony.space.continuous.RandomCartesianAdder;

/**
 * @author encima
 *
 */
public class SensorNetBuilder extends DefaultContext<Object> implements ContextBuilder<Object> {
	ContinuousSpace<Object> space;

	@Override
	public Context<Object> build(Context<Object> context) {
		
		context.setId("K-HAS");
		
//		Geography<Object> geography = GeographyFactoryFinder.createGeographyFactory(null)
//				.createGeography("Geography", context, new GeographyParameters<Object>());
		
		NetworkBuilder<Object> netbuild = new NetworkBuilder<Object>("sensor network", context, true);
		netbuild.buildNetwork();
		
		Parameters params = RunEnvironment.getInstance().getParameters();
		int height = (Integer) params.getValue("height");
		int width = (Integer) params.getValue("width");
		int daNodeCount = (Integer) params.getValue("daNodeCount");
		int dpNodeCount = (Integer) params.getValue("dpNodeCount");
		int dcNodeCount = (Integer) params.getValue("dcNodeCount");
		
		ContinuousSpaceFactory spaceFactory = ContinuousSpaceFactoryFinder.createContinuousSpaceFactory(null);
		space = spaceFactory.createContinuousSpace("space", context, new RandomCartesianAdder<Object>(), 
				new repast.simphony.space.continuous.StrictBorders(), height, width);
		
		Vector<NdPoint> daLocs = new Vector<NdPoint>();
		int i = 0;
		for(; i < daNodeCount; i++) {
			DANode dan = new DANode(i, "wi-fi", 2097, space);
			context.add(dan);
			daLocs.add(space.getLocation(dan));
		}
		
		Vector<NdPoint> dpLocs = new Vector<NdPoint>();
		for(; i < dpNodeCount; i++) {
			DPNode dpn = new DPNode(i, "RF", 2097, space, daLocs);
			context.add(dpn);
			dpLocs.add(space.getLocation(dpn));
		}
		
		
		for(; i < dcNodeCount; i++) {
			DCNode n = new DCNode(i, "RF", 2097, space, dpLocs);
			context.add(n);
		}
		
		System.out.println("***********Initialised Network***********");
		
		
		return context;
	}	

}
