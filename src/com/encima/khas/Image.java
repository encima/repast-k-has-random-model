package com.encima.khas;

import java.util.Date;

public class Image {
	
	int takenBy;
	int takenAt;
	int size;
	String name;
	
	int minSize = 6970;
	int maxSize = 8660;
	
	public Image(int takenBy, int takenAt, String name) {
		super();
		this.takenBy = takenBy;
		this.takenAt = takenAt;
		this.size = (minSize + (int)(Math.random() * ((maxSize - minSize) + 1)));
		this.name = name;
	}

	public int getTakenBy() {
		return takenBy;
	}

	public void setTakenBy(int takenBy) {
		this.takenBy = takenBy;
	}

	public int getTakenAt() {
		return takenAt;
	}

	public void setTakenAt(int takenAt) {
		this.takenAt = takenAt;
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
}
