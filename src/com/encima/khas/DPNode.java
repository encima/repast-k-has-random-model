/**
 * 
 */
package com.encima.khas;

import java.util.Iterator;
import java.util.Random;
import java.util.Vector;

import repast.simphony.engine.environment.RunEnvironment;
import repast.simphony.engine.schedule.ScheduledMethod;
import repast.simphony.space.continuous.ContinuousSpace;
import repast.simphony.space.continuous.NdPoint;

/**
 * @author encima
 *
 */
public class DPNode extends Node {

	Vector<NdPoint> daLocs;
	private boolean processing;
	/**
	 * @param nodeID
	 * @param transMethod
	 * @param maxStorage
	 * @param space
	 */
	public DPNode(int nodeID, String transMethod, int maxStorage,
			ContinuousSpace<Object> space, Vector<NdPoint> daLocs) {
		super(nodeID, transMethod, maxStorage, space);
		this.daLocs = daLocs;
	}
	
//	based on the assumption that the node only has power to process one set at a time
	public void processSet(DwCArchive received) {
		DwCArchive dwc = received;
		if(!dwc.isProcessed()) {
			this.setProcessing(true);
			Vector<Image> images = dwc.getImages();
			Image proc = null;
//			based on a 30% chance of an image containing an object, this is not specific to location but taken across a whole set
			if(new Random().nextDouble() <= 0.3) {
//				proc = images.get(0);
//				proc.setImageOf("INTERESTING");
				dwc.setContents("INTERESTING");
				dwc.setProcessedImage(proc);
			}else{
//				images.get(0).setImageOf("EMPTY");
				dwc.setContents("EMPTY");
//				Add an empty boolean to the darwin core archive class
			}
			dwc.setProcessed(true);
			this.setProcessing(false);
		}
	}
	
	@ScheduledMethod (start = 10, interval = 10000, priority = 1.7976931348623157E308d)
	public void sendSet() {
		if(this.getSendState() != null && this.getReceiver() != null) {
			int tick = (int) RunEnvironment.getInstance().getCurrentSchedule().getTickCount();
//			On preparation of sending the file, calculate the send time from the start
			DwCArchive toSend = (DwCArchive) this.getSendState().getFile();
			if(this.getSendState().getTransTime() == -1) {
				int transTime = (int) (tick + (toSend.getSize() / this.getTransRate()));
				this.getSendState().setTransTime(transTime);
			} else if(this.getSendState().getTransTime() <= tick) {
				this.getReceiver().receive(toSend, this.getSendState().getTransTime());
				archives.remove(toSend);
				this.setSendState(null);
			}
		}else if(this.archives.size() > 0){
//			Add first archive (if not empty) and replace with interesting (if available)
			this.setSendState(new SendState(this.archives.firstElement(), -1));
			for(DwCArchive dwc : this.archives) {
				if(dwc.isProcessed() && dwc.getContents().equals("INTERESTING")) {
					this.setSendState(new SendState(dwc, -1));
				}
			}
		}
	}

	boolean isProcessing() {
		return processing;
	}

	void setProcessing(boolean processing) {
		this.processing = processing;
	}
	
}
