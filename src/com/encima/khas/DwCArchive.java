/**
 * 
 */
package com.encima.khas;

import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

/**
 * @author encima
 *
 */
public class DwCArchive {
	
	Vector<Image> images;
	Image processedImage;
	Map<Integer, Node> route = new HashMap<Integer, Node>();
	private String contents;
	int size = 0;
	boolean processed = false;
//	assumed to have content, must be accompanied with processed
	private boolean empty = false;
	int created = 0;
	
	public DwCArchive(Vector<Image> images, Node orig) {
//		super();
		this.images = images;
		this.processedImage = null;
		int total = 0;
		for(Image image : images) {
			total += image.getSize();
		}
		this.setSize(total);
		this.route.put(0, orig);
	}
	
	public DwCArchive(Vector<Image> images, Image proc, Node orig) {
//		super();
		images = new Vector<Image>();
		this.images = images;
		this.processedImage = proc;
		int total = 0;
		for(Image image : images) {
			total += image.getSize();
		}
		total += proc.getSize();
		this.setSize(total);
		this.route.put(0, orig);
	}


	protected Vector<Image> getImages() {
		return images;
	}

	protected void setImages(Vector<Image> images) {
		this.images = images;
	}

	protected Image getProcessedImage() {
		return processedImage;
	}

	protected void setProcessedImage(Image processedImage) {
		this.processedImage = processedImage;
	}

	protected int getSize() {
		return size;
	}

	protected void setSize(int size) {
		this.size = size;
	}

	protected boolean isProcessed() {
		return processed;
	}

	protected void setProcessed(boolean processed) {
		this.processed = processed;
	}

	String getContents() {
		return contents;
	}

	void setContents(String contents) {
		this.contents = contents;
	}
	
	protected void addRoute(Node n) {
		this.route.put(this.route.size(), n);
	}

	boolean isEmpty() {
		return empty;
	}

	void setEmpty(boolean empty) {
		this.empty = empty;
	}

}
